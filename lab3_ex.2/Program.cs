﻿using System;

namespace lab3_ex._2
{
    class Program
    {
        static void Main(string[] args)
        {

            {
                // f(x, y, z) = !x|!z&z&y = true
                bool x = false;
                bool z = true;
                bool y = true;

                var f = !x | !z & z & y;

                Console.WriteLine(f);
            }

            {
                bool x = false;
                bool z = false;
                bool y = false;

                var f1 = !x | !z & z & y;

                Console.WriteLine(f1);
            }

            {
                bool x = false;
                bool z = false;
                bool y = true;
                
                var f2 = !x | !z & z & y;


                Console.WriteLine(f2);
            }

            {
                bool x = false;
                bool z = true;
                bool y = false;

                var f3 = !x | !z & z & y;

                Console.WriteLine(f3);

            }

            {
                bool x = true;
                bool z = true;
                bool y = true;

                var f4 = !x | !z & z & y;

                Console.WriteLine(f4);

            }

            {
                bool x = true;
                bool z = false;
                bool y = true;

                var f5 = !x | !z & z & y;

                Console.WriteLine(f5);
            }

            {
                bool x = true;
                bool z = false;
                bool y = false;

                var f6 = !x | !z & z & y;

                Console.WriteLine(f6);
            }

            {
                bool x = true;
                bool z = true;
                bool y = false;

                var f7 = !x | !z & z & y;

                Console.WriteLine(f7);
            }
        }
    }
}
