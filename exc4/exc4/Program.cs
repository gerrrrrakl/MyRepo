﻿using System;

namespace exc4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("цифровая модель комнаты");

            // Заданные атрибуты цифровой модели
            // Периметр комнаты
            double AveragePerimeter;
            // Высота потолка
            double Height;
            // Количество столов
            int TableCount;
            // Количество стульев
            int ChairsCount;
            // Количество окон
            int WindowsCount;

            // Вычисляемые атрибуты цифровой модели
            // Площадь дерева
            double Square;

            // Ввод информации в программу.
            Console.WriteLine("Введите AveragePerimeter:");
            string AveragePerimeterStr = Console.ReadLine();
            AveragePerimeter = Convert.ToDouble(AveragePerimeterStr);

            Console.WriteLine("Введите Height:");
            string HeightStr = Console.ReadLine();
            Height = Convert.ToDouble(HeightStr);

            Console.WriteLine("Введите TableCount:");
            string TableCountStr = Console.ReadLine();
            TableCount = Convert.ToInt32(TableCountStr);

            Console.WriteLine("Введите ChairsCount:");
            string ChairsCountStr = Console.ReadLine();
            ChairsCount = Convert.ToInt32(ChairsCountStr);

            Console.WriteLine("Введите WindowsCount:");
            string WindowsCountStr = Console.ReadLine();
            WindowsCount = Convert.ToInt32(WindowsCountStr);

            // Расчет вычисляемых атрибутов
            // Вводим переменную B, чтоб упростить вычисления, т.к. соотношение используется несколько раз.
            double B = AveragePerimeter / (2 * Math.PI);
            Square = Math.PI * B * B;

            // Вывод информации на консоль.
            Console.WriteLine("AveragePerimeter: " + AveragePerimeter + " м.");
            Console.WriteLine("Height: " + Height + " м.");
            Console.WriteLine("TableCount: " + TableCount);
            Console.WriteLine("ChairsCount: " + ChairsCount);
            Console.WriteLine("WindowsCount: " + WindowsCount);
            Console.WriteLine("Square: " + Square + " м^2.");
        }
    }
}
