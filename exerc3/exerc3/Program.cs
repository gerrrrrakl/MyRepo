﻿using System;

namespace exerc3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //f(x, y, z) = 1/21 + ln(|y3/4|) + 7z/2 + min(2, x)

            Console.WriteLine("Введите x:");

            Console.WriteLine("Введите y:");

            Console.WriteLine("Введите z:");

            string xstr = Console.ReadLine();

            string ystr = Console.ReadLine();

            string zstr = Console.ReadLine();

            double x = Convert.ToDouble(xstr);

            double y = Convert.ToDouble(ystr);

            double z = Convert.ToDouble(zstr);

            double f;
            f = 1 / 21.0 + Math.Log(Math.Abs(y * 3/4)) + 7*z/2 + Math.Min(2, x);

            Console.WriteLine(f);



        }
    }
}
