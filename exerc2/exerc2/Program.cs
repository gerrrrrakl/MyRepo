﻿using System;

namespace exerc2
{
    class Program
    {
        static void Main(string[] args)
        {
            //f(x) = 1 / 21 + ln(| 3x / 4 |) + 7 * x / 2 + min(2, x) + xcos(x / 3) + x ^ (x / 3) / 3

            Console.WriteLine("Введите x:");

            string xstr = Console.ReadLine();

            double x = Convert.ToDouble(xstr);

            double y;
            y = 1 / 21.0 + Math.Log(Math.Abs(3*x / 4)) + 7 * x / 2 + Math.Min(2, x) + x*Math.Cos(x / 3) + Math.Pow(x, x/3) / 3;

            Console.WriteLine("f(x)=" + y);

        }
    }
}
