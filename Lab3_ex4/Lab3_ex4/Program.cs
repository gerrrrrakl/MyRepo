﻿using System;

namespace Lab3_ex4
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, m, g, n1, m1, g1;
            Console.WriteLine("Введите день");
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите месяц");
            m = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите год");
            g = Convert.ToInt32(Console.ReadLine());
            n1 = n;
            m1 = m;
            g1 = g;

            //пред день
            switch (n)
            {
                case 1:

                    switch (m)
                    {
                        case 1:
                            n = 31;
                            m = 12;
                            g--;
                            break;
                        default:
                            m--;
                            switch (m)
                            {
                                case 1:
                                case 3:
                                case 5:
                                case 7:
                                case 8:
                                case 10:
                                    n = 31;
                                    break;

                                case 2:
                                    switch (g % 4) //остаток от деления года на 4
                                    {
                                        case 0: //остаток 0 - год високосный
                                            n = 29;
                                            break;
                                        default:
                                            n = 28;
                                            break;
                                    }
                                    break;
                                default:
                                    n = 30;
                                    break;
                            }
                            break;
                    }
                    break;
                default:
                    n--;
                    break;
            }


            //следед день
            switch (n1)
            {
                case 31: // 31 число
                    switch (m1)
                    {
                        case 12: //декабрь
                            n1 = 1;
                            m1 = 1;
                            g1++;
                            break;
                        default:

                            switch (m1)
                            {
                                // мес с 31 днем
                                case 1:
                                case 3:
                                case 5:
                                case 7:
                                case 8:
                                case 10:
                                    n1 = 1;
                                    m1++;
                                    break;
                                default:
                                    Console.WriteLine($"В {m1} МЕСЯЦЕ НЕ БЫВАЕТ 31 ЧИСЛА!");
                                    n1 = 0;
                                    break;
                            }
                            break;
                    }
                    break;


                case 30: //30 число
                    switch (m1)
                    {
                        // мес с 30 дн
                        case 2:
                            Console.WriteLine($"В {m1} МЕСЯЦЕ НЕ БЫВАЕТ 30 ЧИСЛА!");
                            n1 = 0;
                            break;
                        case 4:
                        case 6:
                        case 9:
                        case 11:
                            n1 = 1;
                            m1++;
                            break;
                        default:
                            n1++;
                            break;
                    }
                    break;
                case 28: //28 число
                    switch (m1)
                    {
                        case 2: //февраль
                            switch (g1 % 4) // остаток от деления года на 4
                            {
                                case 0: //остаток 0 - год високосный
                                    n1++;

                                    break;
                                default:
                                    n1 = 1;
                                    break;
                            }
                            break;
                    }
                    break;


                case 29: //29 число
                    switch (m1)
                    {
                        case 2:
                            switch (g1 % 4)
                            {
                                case 0: //остаток 0 - год високосный
                                    n1 = 1;
                                    m1++;
                                    break;
                                default:
                                    Console.WriteLine($"В {g1} году НЕ БЫВАЕТ 29 ФЕВРАЛЯ!");
                                    n1 = 0;
                                    break;
                            }
                            break;
                    }
                    break;


                default:
                    n1++;
                    break;

            }

            switch (n1)
            {
                case 0:
                    Console.ReadLine();
                    break;
                default:
                    Console.WriteLine("Предыдущий день от заданого числа {0}.{1}.{2}", n, m, g);
                    Console.WriteLine("Следующий день от заданого числа {0}.{1}.{2}", n1, m1, g1);
                    Console.ReadLine();
                    break;
            }

        }
    }
}
